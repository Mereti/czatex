package com.example.czatex


import retrofit2.Call
import retrofit2.http.*

interface Json {

    @GET("/shoutbox/messages")
    fun getMessages(): Call<List<Message>>


    @POST("/shoutbox/message")
    fun sendMessage(@Body message:  CreateMessage): Call<Message>

    @PUT("/shoutbox/message/{id}")
    fun putMessage(@Path ( "id" ) id: String,@Body message: CreateMessage): Call<Message>

    @DELETE("/shoutbox/message/{id}")
    fun deleteMessage(@Path("id")id:String): Call<Message>
}