package com.example.czatex.ui.gallery

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.czatex.CreateMessage
import com.example.czatex.Json
import com.example.czatex.Message
import com.example.czatex.R
import com.example.czatex.ui.slideshow.SwipeToDeleteCallback
import kotlinx.android.synthetic.main.fragment_gallery.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class GalleryFragment : Fragment(), MessageAdapter.ClickList, SwipeRefreshLayout.OnRefreshListener{

    private lateinit var galleryViewModel: GalleryViewModel
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as AppCompatActivity).supportActionBar?.show()
        (activity as AppCompatActivity).supportActionBar?.setTitle("Shoutbox")
        galleryViewModel = ViewModelProviders.of(this).get(GalleryViewModel::class.java)



        val retrofit = Retrofit.Builder().baseUrl("http://tgryl.pl/").addConverterFactory(
            GsonConverterFactory.create()
        ).build()
        val json = retrofit.create(Json::class.java)
        val login = readData()
        val root = inflater.inflate(R.layout.fragment_gallery, container, false)
        val content = root.findViewById<EditText>(R.id.editText3)
        swipeRefreshLayout = root.findViewById(R.id.refreshLayout)//!
        swipeRefreshLayout.setOnRefreshListener(this)
        ref()
        galleryViewModel.text.observe(viewLifecycleOwner, Observer {
            remes(json)
            val button2 = root.findViewById<Button>(R.id.button2)
            button2.setOnClickListener{
                var myMessage = CreateMessage(content.text.toString(),login!!)
                println(login)
                println(myMessage)
                json.sendMessage(myMessage).enqueue(object : Callback<Message> {
                    override fun onFailure(call: Call<Message>, t: Throwable) {
                       throw t
                    }

                    override fun onResponse(
                        call: Call<Message>,
                        response: Response<Message>
                    ) {
                        remes(json)
                        println("wykonalo sie ......")
                        println(response.code())
                    }

                })

            }
        })
        return root
    }
    fun remes(json: Json) {
        val call = json.getMessages()
        call.enqueue(object : Callback<List<Message>> {
            override fun onResponse(
                call: Call<List<Message>?>, response: Response<List<Message>?>

            ) {
                val sharedPref = activity?.getPreferences(
                    Context.MODE_PRIVATE)
                listOfcom.apply {
                    layoutManager = LinearLayoutManager(activity)

                    adapter = MessageAdapter(response.body()?.toMutableList()!!,sharedPref!!,fragmentManager!!)

                }
                //-------------------------------------------------------------------
                listOfcom.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
                val swipeHandler = object : SwipeToDeleteCallback(requireContext(),readData()!!) {
                    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                        val adapter = listOfcom.adapter as MessageAdapter
                           adapter.deleteMess(viewHolder.adapterPosition)
                    }
                }
                val itemTouchHelper = ItemTouchHelper(swipeHandler)
                itemTouchHelper.attachToRecyclerView(listOfcom)
//-------------------------------------------------------------------
                swipeRefreshLayout.isRefreshing=false
            }

            override fun onFailure(call: Call<List<Message>>, t: Throwable) {
                println("loll- BLADDDDDDDDDDDDDDDDD")
                swipeRefreshLayout.isRefreshing=false
            }
        })
    }
    private fun readData(): String? {
        val sharedPref = activity?.getPreferences(
             Context.MODE_PRIVATE)
        return sharedPref?.getString(getString(R.string.login_key), getString(R.string.user))

    }

    override fun clickList(position: Int) {

    }

    override fun onRefresh() {
        val retrofit = Retrofit.Builder().baseUrl("http://tgryl.pl/").addConverterFactory(
            GsonConverterFactory.create()
        ).build()
        val json = retrofit.create(Json::class.java)
        remes(json)



    }

    private fun ref(){
     val r = object : Runnable {

            override fun run() {
                if(swipeRefreshLayout.isRefreshing()){
                    onRefresh()
                    swipeRefreshLayout.isRefreshing=false
                    Handler().postDelayed(this,30000)
                }
            }

        }
        Handler().postDelayed(r,30000)


    }
}
