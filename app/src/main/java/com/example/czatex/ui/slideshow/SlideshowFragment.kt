package com.example.czatex.ui.slideshow

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.example.czatex.CreateMessage
import com.example.czatex.Json
import com.example.czatex.Message
import com.example.czatex.R
import com.example.czatex.ui.gallery.GalleryFragment
import com.example.czatex.ui.gallery.MessageAdapter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.example_coment.*
import kotlinx.android.synthetic.main.fragment_gallery.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter


class SlideshowFragment : Fragment() {

    private lateinit var slideshowViewModel: SlideshowViewModel

    private lateinit var name: TextView
    private lateinit var time: TextView
    private lateinit var myMessage: EditText
    private lateinit var editTextButton :Button
    private lateinit var  deleteButton : Button

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        //-------------------------------PUT
        val retrofit = Retrofit.Builder().baseUrl("http://tgryl.pl/").addConverterFactory(
            GsonConverterFactory.create()
        ).build()
        val json = retrofit.create(Json::class.java)



        slideshowViewModel =
                ViewModelProviders.of(this).get(SlideshowViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_slideshow, container, false)

        name = root.findViewById(R.id.name)
        time = root.findViewById(R.id.time)
        myMessage = root.findViewById(R.id.message)
        editTextButton = root.findViewById(R.id.saveMessageBtn)
        deleteButton = root.findViewById(R.id.deleteButton)

        val message= readData()
        name?.text = message.login.toString()
        time?.text = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss").format(ZonedDateTime.parse(message.date))
        myMessage?.text= Editable.Factory.getInstance().newEditable(message.content)
        println(message.content)

        slideshowViewModel.text.observe(viewLifecycleOwner, Observer {

        })

        //-------------------------------------------------------------------
            deleteButton.setOnClickListener{
                json.deleteMessage(message.id).enqueue(object : Callback<Message> {
                    override fun onFailure(call: Call<Message>, t: Throwable) {
                        throw t
                    }

                    override fun onResponse(
                        call: Call<Message>,
                        response: Response<Message>
                    ) {
                        val bundle = Bundle()
                        val fragment: Fragment = GalleryFragment()
                        fragment.arguments = bundle
                        val fragmentManager: FragmentManager? = fragmentManager
                        fragmentManager?.beginTransaction()?.replace(R.id.nav_host_fragment, fragment)?.commit()
                        println("wykonalo sie ......")
                        println(response.code())
                    }

                })
            }
        //--------------------------------------------------------------------
        editTextButton.setOnClickListener{
            var myMessage = CreateMessage(myMessage.text.toString(),message.login)
            println(login)
            println(myMessage)
            json.putMessage(message.id,myMessage).enqueue(object : Callback<Message> {
                override fun onFailure(call: Call<Message>, t: Throwable) {
                    throw t
                }

                override fun onResponse(
                    call: Call<Message>,
                    response: Response<Message>
                ) {
                    val bundle = Bundle()
                    val fragment: Fragment = GalleryFragment()
                    fragment.arguments = bundle
                    val fragmentManager: FragmentManager? = fragmentManager
                    fragmentManager?.beginTransaction()?.replace(R.id.nav_host_fragment, fragment)?.commit()
                    println("wykonalo sie ......")
                    println(response.code())
                }

            })

        }
        return root
    }
    private fun readData(): Message {
        val sharedPref = activity?.getPreferences(
            Context.MODE_PRIVATE)
        val gson = Gson()
        val json = sharedPref?.getString("K1","")
        return gson.fromJson<Message>(json, Message::class.java)
    }

}
