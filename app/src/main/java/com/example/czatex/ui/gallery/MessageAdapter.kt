package com.example.czatex.ui.gallery


import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.czatex.Json
import com.example.czatex.Message
import com.example.czatex.R
import com.example.czatex.ui.slideshow.SlideshowFragment
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter


class MessageAdapter(private val list: MutableList<Message>, val sharedPreferences: SharedPreferences,val fragmentManager: FragmentManager) : RecyclerView.Adapter<MessageViewHolder>() {

     fun deleteMess(position:Int){
         val message:Message = list.get(position)
         list.removeAt(position)
         notifyItemRemoved(position)
         val retrofit = Retrofit.Builder().baseUrl("http://tgryl.pl/").addConverterFactory(
             GsonConverterFactory.create()
         ).build()
         val json = retrofit.create(Json::class.java)
         val call = json.deleteMessage(message.id)
         call.enqueue(object : Callback<Message>{
             override fun onFailure(call: Call<Message>, t: Throwable) {

             }

             override fun onResponse(call: Call<Message>, response: Response<Message>) {

             }

         })


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MessageViewHolder(inflater, parent,sharedPreferences,fragmentManager)
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val message: Message = list[position]
        holder.bind(message)

    }

    override fun getItemCount(): Int = list.size

    interface ClickList {
        fun clickList(position: Int)
    }

}

class MessageViewHolder(inflater: LayoutInflater, parent: ViewGroup, val sharedPreferences: SharedPreferences, val fragmentManager: FragmentManager) : RecyclerView.ViewHolder(inflater.inflate(R.layout.example_coment, parent, false)), View.OnClickListener {

    private var login: TextView? = null
    private var date : TextView? = null
    private var comment: TextView? = null


    //private var  mContext: Context;
   // @RequiresApi(Build.VERSION_CODES.O)
   // val formater = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss")


    init {
        login = itemView.findViewById(R.id.login)
        date = itemView.findViewById(R.id.date)
        comment = itemView.findViewById(R.id.comment)
    }

    fun getLogin():String{
        return login?.text.toString()


    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun bind(message: Message) {

        login?.text = message.login.toString()
        date?.text = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss").format(ZonedDateTime.parse(message.date))
        comment?.text = message.content.toString()
        itemView.setOnClickListener{
               itemClicked(message)
        }


    }


    override fun onClick(v: View?) {

    }
    private fun itemClicked(message: Message) {
        // Toast.makeText(this, "Clicked: ${message}", Toast.LENGTH_SHORT).show()
        println("PYTAJNIKIII!!!")
       val login : String =sharedPreferences.getString("Login Key","def")!!
        if(message.login.equals(login)){
            val gson = Gson()
            val json = gson.toJson(message)
            sharedPreferences.edit().putString("K1",json).commit()

            val bundle = Bundle()
            val fragment: Fragment = SlideshowFragment()
            fragment.arguments = bundle
            val fragmentManager: FragmentManager? = fragmentManager
            fragmentManager?.beginTransaction()?.replace(R.id.nav_host_fragment, fragment)?.commit()

        }



    }


}

