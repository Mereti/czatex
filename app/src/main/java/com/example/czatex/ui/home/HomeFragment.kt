package com.example.czatex.ui.home

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.czatex.R
import com.example.czatex.ui.gallery.GalleryFragment
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.w3c.dom.Text

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var editText: EditText

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        (activity as AppCompatActivity).supportActionBar?.hide()
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        editText = root.findViewById(R.id.editText)
        readData()
        val button = root.findViewById<Button>(R.id.button)
        homeViewModel.text.observe(viewLifecycleOwner, Observer {

            button.setOnClickListener {

                saveData()
                val bundle = Bundle()
                val context : ConnectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val activeNetwork : NetworkInfo? = context.activeNetworkInfo
                val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
                val text : String = "Internet Connection rquired!!!"


                if (isConnected == true){
                    val fragment: Fragment = GalleryFragment()
                    fragment.arguments = bundle
                    val fragmentManager: FragmentManager? = fragmentManager
                    fragmentManager?.beginTransaction()
                        ?.replace(R.id.nav_host_fragment, fragment)
                        ?.commit()
                }else{
                    println("BRAK POLACZENIA")
                    val toast = Toast.makeText(getContext(), text, Toast.LENGTH_LONG)
                    toast.show()
                }


            }

        })
        return root
    }
    private fun readData(){
        val sharedPref = activity?.getSharedPreferences(
            getString(R.string.name), Context.MODE_PRIVATE)
        sharedPref?.getString(R.string.login_key.toString(), R.string.user.toString())
        }
    private fun saveData(){

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString(getString(R.string.login_key), editText.text.toString())
            commit()
        }

    }

}
